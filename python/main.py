import logging
import os
from shutil import copyfile
import stat 

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def handler(event):
    output = {}
    for env in os.environ:
        output[env] = os.environ[env]

    return output

if __name__ == '__main__':
    handler({'inputParameters': [], 'secretParameters': []})
