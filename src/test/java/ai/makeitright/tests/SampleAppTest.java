package ai.makeitright.tests;

import io.appium.java_client.android.AndroidDriver;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import ai.makeitright.utils.DriverConfig;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;

public class SampleAppTest extends DriverConfig {

    private static final Logger logger = Logger.getLogger(SampleAppTest.class.getName());

    @Test
    public void checkText() throws InterruptedException, IOException {
        System.out.println("******************************Environment Vars*****************************");
        Map<String, String> enviorntmentVars  = System.getenv();
        
        Set<Map.Entry<String, String>> entries = enviorntmentVars.entrySet();
        Iterator<Map.Entry<String, String>> environmentsIterator = entries.iterator();
        while (environmentsIterator.hasNext()) {
            Map.Entry<String, String> environment = environmentsIterator.next();
            System.out.println("Key: " + environment.getKey() + " Value: " + environment.getValue());
        }

        System.out.println("*******************************Finish env Vars*****************************");
        
        logger.info("SampleAppTest.checkText... +1");

        final URL url = new URL("http://127.0.0.1:4723/wd/hub");

        final DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

        final AndroidDriver driver = new AndroidDriver(url, desiredCapabilities);

        Thread.sleep(6000);

        final WebElement button_first = driver.findElementById("pl.makeitright.appium_sample_app:id/button_first");
        button_first.click();
        Thread.sleep(2000);

        String screenshotFilePath = System.getProperty("SCREENSHOTS_PATH");
        String artifactFilePath = System.getProperty("ARTIFACTS_PATH");

        logger.info("screenshotFilePath=" + screenshotFilePath);
        logger.info("artifactFilePath=" + artifactFilePath);

        File screenshot  = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File(screenshotFilePath + "/Screenshot.jpg"));

        File artifact = new File(artifactFilePath + "/artifact.txt");

        if (artifact.createNewFile()) {
            logger.info("File created: " + artifact.getName());
        } else {
            logger.info("File already exists...");
        }

        System.setProperty("output", "{\"result\":{\"out2\":\"val2\"}}");

        logger.info("DONE");
    }
}
